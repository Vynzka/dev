import tkinter.filedialog
import include.Fonction as modPers

fileToParse = tkinter.filedialog.askopenfilename(title="Ouvrir un fichier...", filetypes=[('XML files', '.xml')])

try:
    modPers.toParse(fileToParse)
except:
    print("Error : Something went wrong...")