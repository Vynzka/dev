import xml.etree.ElementTree as ET
from openpyxl import Workbook
import datetime

def toParse(way):
    tree = ET.parse(way)
    root = tree.getroot()
    if root.tag == "RFID_Thales":
        #Création du fichier Excel
        excelFile = Workbook()

        #Récup page active
        thalesPage = excelFile.create_sheet("Thales Mat", 0)
        trescalPage = excelFile.create_sheet("Trescal Mat", 1)

        thalesPage['A1'] = "Thales Material"

        thalesPage['B1'] = "Scan du :"
        dateTmp = datetime.date.today()
        date = str(dateTmp)
        thalesPage['C1'] = date

        #Remplissage
        index = 1
        for child in root:
            index += 1
            indexSTR = str(index)
            thalesPage['A'+ indexSTR] = child.attrib.get("Name")
            thalesPage['B' + indexSTR] = child.attrib.get("Color")
            thalesPage['C' + indexSTR] = str(root[index -2].text)

    excelFile.save("xmlParse From_"+date+".xlsx")